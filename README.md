Curation
============

Using tags: Each game can have multiple tags (though restricting to around 3-4 prob makes sense).
In general "presentation" level games should be a) polished  (ie provide an "unbroken" user experience)
				and 	 b) fun

So that we assume that the majority of potential players will enjoy at least trying out the game.

Within the "presentation" level, those principally displayed to players should be as close as we've got to AAA titles. All other games can be advertised / presented if browsing their categories, or in moderation (or both)

So:

Curated Games
=================

AA titles
------------

 * 0AD (*AA*, RTS, Historic)
 * Unvanquished ? (*AA*, FPS MOBA, Sci-Fi)
 * Xonotic (*AA*, Arena FPS, Sci-Fi)
 * Warsow or Red Eclipse?

( Ok, this is a shortish list atm :P. )

Retro
-------------

 * Wesnoth (Retro, Turn-based-Strategy, Fantasy)
 * OpenHV (Retro, RTS, Sci-Fi)
 * Widelands (Retro, City-Building, RTS, Historic)
 * OpenTTD ? (Retro, Tycoon game, Present)
 * Flame: Empyrean Campaign (Retro, Hack and Slash, Fantasy)
 * Shattered Pixel Dungeon (Retro, RPG, Roguelike, Fantasy)


Mobile ? Good category ? (or include in "Action? Not quite maybe?)
--------------------------------------------------------------------

 * Mindustry ( , Tower Defense, Building Sim, Sci-Fi)


Voxel Games
--------------

 * Veloren (Voxel, RPG, Open-World, Fantasy)


Fun? / Action ?
-----------------

 * Teeworlds ( Action, 2D-FPS / Platform shooter? ?, Cute / Comic ? :P)
 * Hedgewars ( Action, Turn-taking shooter, Comic/Cartoon ? )
 * SuperTuxKart (Action, Kart Game, Cartoon )

Arcade
-----------

 * Chromium B.S.U (Arcade, Top-Scroller, Sci-Fi)


As yet Uncategorised (but good candidates for "presentation" level, I think!):
-----------------------------------------------------------------------------------

 * FreeOrion ( ? , 4X, Sci-Fi)
 * Freeciv ( ? , Turn-based-Strategy, Historic)
 * Warzone 2100 ( ? , RTS, Sci-Fi)



In Development (ie everything else).
=======================================

And then of course, we have a whole world of games I'd classify as "in development" (think "early access" in Steam, if you're familiar with that). That is, I'd definitely want to have them on site, but since they're not really "ready to play" (at least from a casual players view), there should be mechanisms to hide them from "accidental" discovery (I'd be interested in what Steam does here, actually).




## Author
Sean Scherer, with help from Libre Games Lists by "Rampoina" (eg here: https://rampoina.gitlab.io/libregameslist/ )

## License
For open source projects, say how it is licensed.

GPLv3 or later, CC-BY-SA (need to link I guess..)
